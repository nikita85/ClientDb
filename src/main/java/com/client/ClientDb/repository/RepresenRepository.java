package com.client.ClientDb.repository;

import com.client.ClientDb.hello.Representative;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by admin on 15.08.2018.
 */
public interface RepresenRepository  extends CrudRepository<Representative, Long> {
}
