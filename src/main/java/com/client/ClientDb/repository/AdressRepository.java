package com.client.ClientDb.repository;

import com.client.ClientDb.hello.Adres1;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by admin on 16.08.2018.
 */
public interface AdressRepository extends CrudRepository<Adres1,Long> {
}
