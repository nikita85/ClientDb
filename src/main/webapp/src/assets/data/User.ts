export interface User {
  id: number;
  name: string;
  country: string;
  status: string;
  addDate: string;
  updateDate: string;
  website: string;
  type1: string;
  comments: string;
}
