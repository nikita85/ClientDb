export interface Representative {
  id: number;
  name: string;
  lastname: string;
  statusRep: string;
  email: string;
  addDate: string;
  updateDate: string;


}
