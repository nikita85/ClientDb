import {Component, OnInit} from '@angular/core';
import {TableService} from '../../../services/table.service';

@Component( {
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: [ './table.component.css' ],
  providers: [ TableService ]
} )
export class TableComponent implements OnInit {
  dataToDisplay;

  constructor( private tableService: TableService ) {
  }

  ngOnInit() {
    this.getAll();
  }



  Delete1(id: number) {
    this.tableService.getDelete(id).subscribe(() => {
      this.getAll();
    });
  }



  private getAll() {
    this.tableService.getUser()
      .subscribe((data) => {
        this.dataToDisplay = data;
      });
  }

}
