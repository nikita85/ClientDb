import {Component, OnInit} from '@angular/core';
import {RepService} from '../../../services/represantative.service';

@Component( {
  selector: 'app-represantative',
  templateUrl: './represantativeTable.component.html',
  styleUrls: [ './represantativeTable.component.css' ],


} )
export class RepresantativeComponent implements OnInit {

  dataToDisplay;

  constructor( private tableService: RepService ) {
  }

  ngOnInit() {
    this.getAll();
  }

  Delete( id: number ) {
    this.tableService.getDelete1(id).subscribe(() => {
      this.getAll();
    });


  }


  private getAll() {
    this.tableService.getUser()
      .subscribe((data) => {
        this.dataToDisplay = data;
      });
  }

}
