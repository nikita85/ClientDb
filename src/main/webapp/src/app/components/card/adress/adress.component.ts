import {Component, OnInit} from '@angular/core';
import {AddresService} from '../../../services/adres.service';

@Component( {
  selector: 'app-adress',
  templateUrl: './adress.component.html',
  styleUrls: [ './adress.component.css' ]
} )
export class AdressComponent implements OnInit {

  dataToDisplay;


  constructor( private tableService: AddresService ) {
  }

  ngOnInit() {
    this.getAll();
  }
  Delete( id: number ) {

    this.tableService.getDelete2(id).subscribe(() => {
      this.getAll();
    });


  }

  private getAll() {
    this.tableService.getUser()
      .subscribe( ( data ) => {
        this.dataToDisplay = data;
      } );
  }

}

