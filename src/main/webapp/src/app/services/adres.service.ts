import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AddresService {

  constructor( private http: HttpClient ) {
  }

  getUser() {
    return this.http.get( '/api/adress/all1' );
  }

  getDelete2(id: number) {
    return this.http.delete( '/api/adress/notes/' + id );

  }

  getByid2(id: number) {
    return this.http.get('/api/adress/' + id);
  }
}
