import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class RepService {

  constructor(private http: HttpClient) {}

  getUser() {
    return this.http.get('/api/rep/alll');
  }

  getDelete1(id: number) {
    return this.http.delete( '/api/rep/notes/' + id );

  }
  getByid1(id: number) {
    return this.http.get('api/rep/' + id);
  }
}
