import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class TableService {

  constructor( private http: HttpClient ) {
  }

  getUser() {
    return this.http.get( '/api/demo/all' );
  }


  getDelete(id: number) {
    return this.http.delete( '/api/demo/notes/' + id );

  }
  getByid(id: number) {
    return this.http.get('api/demo/' + id);
  }
}
