import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {TableComponent} from './components/card/table/table.component';

import {CommonModule} from '@angular/common';
import {TableService} from './services/table.service';

import {RepresantativeComponent} from './components/card/represantative/represantativeTable.component';
import {HttpClientModule} from '@angular/common/http';
import {RepService} from './services/represantative.service';
import {AdressComponent} from './components/card/adress/adress.component';
import {AddresService} from './services/adres.service';
import { CompnayInfoComponent } from './components/card/table/compnay-info/compnay-info.component';
import {RepInfoComponent} from './components/card/represantative/rep-info/rep-info.component';
import { AdrInfoComponent } from './components/card/adress/adr-info/adr-info.component';


const appRoutes: Routes = [
  { path: 'table', component: TableComponent },
  { path: 'repreTable', component: RepresantativeComponent },
  { path: 'adress2', component: AdressComponent },
  { path: 'info/:id', component: CompnayInfoComponent },
  { path: 'rep-info/:id', component: RepInfoComponent },
  { path: 'adr-info/:id', component: AdrInfoComponent }

];

@NgModule( {
  declarations: [
    AppComponent,
    HeaderComponent,
    TableComponent,
    RepresantativeComponent,
    AdressComponent,
    CompnayInfoComponent,
    RepInfoComponent,
    AdrInfoComponent

  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule
  ],
  providers: [ TableService, RepService, AddresService ],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
